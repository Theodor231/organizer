import { Component } from '@angular/core';
import { AuthService } from './shared/auth.service';
import { TaskService } from './shared/tasks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public authService: AuthService) { }
}
