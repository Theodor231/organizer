import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MomentPipe } from './shared/moemnt.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './shared/auth.service';
import { AuthComponent } from './auth/auth.component';
import { ModalComponent } from './modal/modal.component';

import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { CalendarComponent } from './calendar/calendar.component';
import { SelectorComponent } from './selector/selector.component';
import { OrganizerComponent } from './organizer/organizer.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';


const firebaseConfig = {
  apiKey: "AIzaSyDP5yeUJu2Zgfs-hGDeIePtnLsCRmt9nUA",
  authDomain: "organizer-694ad.firebaseapp.com",
  databaseURL: "https://organizer-694ad.firebaseio.com",
  projectId: "organizer-694ad",
  storageBucket: "organizer-694ad.appspot.com",
  messagingSenderId: "508857087963",
  appId: "1:508857087963:web:597151f823bb407dab59fe"
};

const routes: Routes = [
  {
    path: '',
    // loadChildren: () => import('./main/main.module').then(m => m.MainModule),
    component: MainComponent
  },
  {
    path: 'auth',
    component: AuthComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    CalendarComponent,
    SelectorComponent,
    OrganizerComponent,
    MainComponent,
    MomentPipe,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule // storage

  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
