import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TaskService } from '../shared/tasks.service';

import * as moment from 'moment';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  moment = moment;


  constructor(
    public taskService: TaskService,
    public authService: AuthService,
  ) { }
}
