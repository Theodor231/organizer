import { Component, OnInit } from '@angular/core';
import { TaskService, Task } from '../shared/tasks.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DateService } from '../shared/date.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  difference = ''
  projects = []
  selectedProject
  form: FormGroup;
  types = [
    'Task',
    'Bug',
    'Deploy',
    'Modify'
  ]
  moment = moment;
  constructor(
    private taskService: TaskService,
    private dateService: DateService,
  ) { }

  ngOnInit(): void {
    this.taskService.get_projects().subscribe((project: any[]) => {
      this.projects = project
    })
    this.form = new FormGroup({
      start: new FormControl('', Validators.required),
      end: new FormControl('', Validators.required),
      expected: new FormControl(),
      type: new FormControl('', Validators.required),
      project: new FormControl('', Validators.required),
      description: new FormControl(''),
    })
  }

  get_branch() {
    console.log(this.selectedProject)
  }


  submit() {
    if (this.form.invalid) {
      return
    }

    this.form.value.description = 'new task is for improve yout skills'
    const task: Task = { ...this.form.value, duration: this.difference, date: this.dateService.date.value.format('DD-MM-YYYY') }
    console.log(task);

    this.taskService.create(task).subscribe(task => {
      this.taskService.tasks.push(task)
      this.form.reset()
    }, err => {
      console.error(err);

    })

    this.taskService.showModal = false;
  }

  select_date() {
    const start = moment(this.toTime(this.form.value.start))
    const end = moment(this.toTime(this.form.value.end))

    const differenceInMs = end.diff(start);
    const duration = moment.duration(differenceInMs);
    const differenceInMinutes = duration.asMinutes();

    this.difference = String(this.getTimeFromMins(differenceInMinutes))
  }

  toTime(timeString) {
    var timeTokens = timeString.split(':');
    var date = new Date(1970, 0, 1, timeTokens[0], timeTokens[1], 0);
    return date.toISOString()
  }

  close() {
    this.taskService.showModal = false
  }

  getTimeFromMins(mins) {
    var h = mins / 60 | 0,
      m = mins % 60 | 0;
    return moment.utc().hours(h).minutes(m).format("hh:mm");
  }


}
