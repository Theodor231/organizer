import { Component, OnInit } from '@angular/core';
import { DateService } from '../shared/date.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskService, Task } from '../shared/tasks.service';
import { switchMap } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss']
})
export class OrganizerComponent implements OnInit {

  form: FormGroup;
  moment = moment
  projects = [];
  showBlock = false;

  constructor(
    public dateService: DateService,
    public taskService: TaskService,

  ) { }

  ngOnInit(): void {
    this.dateService.date.pipe(
      switchMap(value => this.taskService.load(value))
    ).subscribe(tasks => {
      console.log(tasks);

      this.taskService.tasks = tasks;
      this.showBlock = true;
    })

    this.form = new FormGroup({
      title: new FormControl('', Validators.required)
    })
    this.taskService.get_projects().subscribe((project: any[]) => {
      this.projects = project;
    })
  }

  remove(task: Task) {
    this.taskService.remove(task).subscribe(() => {
      this.taskService.tasks = this.taskService.tasks.filter(c => c.id !== task.id)
    }, err => console.error(err)
    )
  }

}
