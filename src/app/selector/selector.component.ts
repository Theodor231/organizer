import { Component } from '@angular/core';
import { DateService } from '../shared/date.service';
import { AuthService } from '../shared/auth.service';

import *as moment from 'moment';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent {

  constructor(
    public dateService: DateService,
    public auth: AuthService
  ) { }


  go(dir: number) {
    this.dateService.changeMonth(dir);
  }

}
