import { Injectable, NgZone } from "@angular/core";
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { TaskService } from './tasks.service';
import { HttpClient } from '@angular/common/http';
import { auth } from 'firebase/app'
import * as admin from 'firebase-admin';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user: any
  users: any[] = [];

  isForView = false;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private router: Router,
    private taskService: TaskService,
    private http: HttpClient,
    private ngZone: NgZone
  ) {
    console.log(admin.credential);

  }

  login() {
    this.onLoginWithGoogle()
  }

  onLoginWithGoogle() {
    this.isForView = false
    this.angularFireAuth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider()).then((res) => {
        if (res) {
          this.user = res.user
          console.log(res);

          this.get_user(res.user.uid).subscribe(c => {
            console.log(c);
          })
          localStorage.setItem('token', res.credential['accessToken']);
          localStorage.setItem('user', JSON.stringify(res.user));

          // this.ngZone.run(() => this.router.navigate(['/'])).then();
        }
      })
  }

  get_user(uid) {
    return this.http.get(environment.url + '/test', { params: { uid } })
  }

  FacebookAuth() {
    return this.AuthLogin(new auth.FacebookAuthProvider());
  }

  // Auth logic to run auth providers
  AuthLogin(provider) {
    return this.angularFireAuth.signInWithPopup(provider)
      .then((result) => {
        this.get_user(result.user.uid).subscribe(c => {
          console.log(c);
        })
      }).catch((error) => {
        console.log(error)
      })
  }

  signup(email: string, password: string) {
    this.angularFireAuth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Success!', value);
      })
      .catch(err => {
        console.log('Something went wrong:', err.message);
      });
  }

  getUser(email) {
    // admin.auth().getUserByEmail(email)
    //   .then(function (userRecord) {
    //     // See the UserRecord reference doc for the contents of userRecord.
    //     console.log('Successfully fetched user data:', userRecord.toJSON());
    //   })
    //   .catch(function (error) {
    //     console.log('Error fetching user data:', error);
    //   });
  }
}
