import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as firebase from 'firebase';


@Injectable({
    providedIn: 'root'
})
export class CrudService {



    db = firebase.firestore();

    constructor(
        private firestore: AngularFirestore,
        private http: HttpClient,
    ) { }

    // .orderBy('datetimeFormation', 'desc')

    getData() {
        // return this.firestore.collection('info', ref =>ref.orderBy('datetimeFormation', 'desc')).get();
        return this.firestore.collection('info', ref => ref.orderBy('datetimeFormation', 'desc').limit(28)).get();
    }

    getPagesInfo(id) {
        return this.firestore.collection('info', ref => ref
            .limit(28).orderBy('id', 'desc').where('id', '<', +id)
        ).get()
    }

    postUser(data) {
        return this.firestore.collection('users').add(data);
    }

    getUser(token) {
        return this.firestore.collection('users', ref => ref.where('token', '==', token)).snapshotChanges();
    }

    updateUser(id, data) {
        return this.firestore.collection('users').doc(id).set({
            "token": data["token"],
            "settings": data["settings"],
        });
    }

    getNews() {
        return this.firestore.collection('categories').snapshotChanges();
    }

    getArticleContent(id) {
        return this.firestore.collection('articles').doc(id).get()
        // return this.firestore.collection('articles').where(firebase.firestore.FieldPath.documentId(), '==', id).get()
    }

    getInfoByType(type) {
        // return this.firestore.collection('news', ref => ref.where('type', '==', +type)).snapshotChanges()
        // ref.orderBy('datetimeFormation', 'desc').limit(8)).get()
        return this.firestore.collection('info', ref => ref.orderBy('datetimeFormation', 'desc').where('type', '==', +type).limit(28)).get()
    }

    getInfoByTypeperPage(type, id) {
        return this.firestore.collection('info', ref => ref
            .limit(28).orderBy('id', 'desc').where('id', '<', +id).where('type', '==', +type)
        ).get()
    }

    getInfoContent(id) {
        return this.firestore.collection('info').doc(id).get()
    }

    getFixedInfo() {
        return this.firestore.collection('info', ref => ref.orderBy('datetimeFormation', 'desc').where('isFixed', '==', true).limit(5)).snapshotChanges()
    }

    getLast24hInfo(value) {
        return this.firestore.collection('info', ref => ref.orderBy('datetimeFormation', 'desc').where('datetimeFormation', '>', value)).snapshotChanges()
    }

    getSocials() {
        return this.firestore.collection('socials').snapshotChanges();
    }
    getSubscriptions() {
        return this.firestore.collection('subscritpions').snapshotChanges();
    }

}
