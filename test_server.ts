const express = require('express')
const app = express()
app.use((request, response, next) => {
    console.log(request.headers)
    next()
})
app.use((request, response, next) => {
    request.chance = Math.random()
    next()
})
app.get('/', (request, response) => {
    response.json({
        chance: request.chance
    })
})

var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://organizer-694ad.firebaseio.com"
});


function getUser(email) {
    admin.auth().getUserByEmail(email)
        .then(function (userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully fetched user data:', userRecord.toJSON());
        })
        .catch(function (error) {
            console.log('Error fetching user data:', error);
        });
}


function listAllUsers() {
    // List batch of users, 1000 at a time.
    admin.auth().listUsers()
        .then(function (listUsersResult) {
            listUsersResult.users.forEach(function (userRecord) {
                console.log('user', userRecord.toJSON().email);
            });
        })
        .catch(function (error) {
            console.log('Error listing users:', error);
        });
}
// Start listing users from the beginning, 1000 at a time.
listAllUsers();

app.get('/test', (request, response) => {
    listAllUsers()
    response.send(
        // getUser('teisanutudort@gmail.com')
    )
})

app.listen(3000)